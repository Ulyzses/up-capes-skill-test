# UP CAPES Skill Test 2022-2023

## IT Web Development Checklist
- [X] Basics
- [X] Collaboration
- [X] Front-end Development
- [X] Back-end Development
- [X] Automation