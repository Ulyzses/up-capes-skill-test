// I don't wanna import jQuery
const $ = document.querySelector.bind(document)
const $id = document.getElementById.bind(document)
const $class = document.getElementsByClassName.bind(document)


// Yeeted from StackOverflow
function isAlphaNumeric(str) {
  var code, i, len;

  for (i = 0, len = str.length; i < len; i++) {
    code = str.charCodeAt(i);
    if (!(code > 47 && code < 58) && // numeric (0-9)
        !(code > 64 && code < 91) && // upper alpha (A-Z)
        !(code > 96 && code < 123)) { // lower alpha (a-z)
      return false;
    }
  }
  return true;
};


function showError(elem, msg) {
  parent = elem.parentNode
  parent.classList.add('error')

  errorBox = document.createElement('div')
  errorBox.className = 'form-text error-box'
  errorBox.innerText = msg

  parent.appendChild(errorBox)
}


$('form').addEventListener('submit', e => {
  e.preventDefault()
  
  // Clear validation messages (if any)
  Array.from($class('error')).forEach(elem => {
    elem.classList.remove('error')
  })

  Array.from($class('error-box')).forEach(elem => {
    elem.parentNode.removeChild(elem)
  })

  // Get values
  fields = e.target.elements
  const username = fields.username.value
  const password = fields.password.value
  const remember = fields.remember.checked

  // Validate Username
  if (!isAlphaNumeric(username)) {
    showError($id('username'), "Username must contain only alphanumeric characters")
  }

  // Validate Password
  if (password.length < 8) {
    showError($id('password'), "Password must be at least 8 characters long")
  }
})


$id('toggle-password').addEventListener('click', evt => {
  eye = evt.target

  if (eye.classList.contains('fa-eye')) {
    eye.classList.remove('fa-eye')
    eye.classList.add('fa-eye-slash')
  } else {
    eye.classList.remove('fa-eye-slash')
    eye.classList.add('fa-eye')
  }

  passwordField = eye.previousElementSibling.previousElementSibling
  passwordField.type = passwordField.type === 'password' ? 'text' : 'password'
})

/**
 *    _
 * __(.)<  quacc
 * \___)
 */